﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JSReportGenerator.Enum;
using JSReportGenerator.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JSReportDemo.Api
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        public IJSReportServer ReportServer;
        public IJSReport Report;
        public IHostingEnvironment env;
        public TestController(IJSReportServer _ReportServer, IJSReport _Report, IHostingEnvironment _env)
        {
            ReportServer = _ReportServer;
            Report = _Report;
            env = _env;
        }

        [HttpGet]
        public string TestReport()
        {
            var names = new List<Names>();
            names.Add(new Names() { FirstName = "Aron", LastName = "Consuelo" });
            names.Add(new Names() { FirstName = "Guian", LastName = "Salanguit" });
            names.Add(new Names() { FirstName = "Rossu", LastName = "Belmonte" });

            var pets = new List<Pets>();
            pets.Add(new Pets() { Owner = "Rossu", Name = "Dog" });
            pets.Add(new Pets() { Owner = "Rossu", Name = "Cat" });
            pets.Add(new Pets() { Owner = "Aron", Name = "Sheep" });
            pets.Add(new Pets() { Owner = "Guian", Name = "Cow" });

            Report.JSReportService = ReportServer.JSReportService;
            Report.ReportPath = Path.Combine(env.WebRootPath, "Templates");
            var pdf = Report.AddDataSource(names).AddSubReport("Pets", pets).GenerateReport("test-excel", ReportRecipe.HtmlToXlsx);
            return pdf;
        }

        public class Names
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }
        public class Pets
        {
            public string Owner { get; set; }
            public string Name { get; set; }
        }
    }
}